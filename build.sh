#!/usr/bin/env bash
set -euo pipefail
echo "$0"
full_path=$(realpath "$0")
dir_path=${full_path%/*}
readarray -t x86_list <<< "$(find x86_64/ -type f -name PKGBUILD | awk -F / '{print $2}')"
for pkgn in "${x86_list[@]}"
do
  cd "${dir_path}"/x86_64/"${pkgn}"
  echo "!! Building package: ${pkgn}"
  makepkg -cf --sign 7B62EC0A30406994
  #find . -mindepth 1 -maxdepth 1 -type d -print0 | xargs -r0 rm -R
done
# vim: ts=2 sw=2 et:


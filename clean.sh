#!/usr/bin/env bash
#find x86_64/*/** -type f -not -name PKGBUILD -not -name "*.install" -delete
cd x86_64 && \
  cd mullvad-vpn-openrc && \
  rm -rvf mullvadvpn-app && \
  rm -rvf mullvadvpn-app-binaries \
  rm -rvf src && \
  cd ../ckb-next-k95-xt && \
  rm -rvf src && \
  rm -rvf pkg && \
  cd ../helio-workstation-stable-git && \
  rm -rvf helio-workstation && \
  rm -rvf JUCE && \
  rm -rvf pkg && \
  rm -rvf sparsepp && \
  rm -rvf src && \
  rm -rvf *.tar && \
  rm -rvf *.zst && \
  rm -rvf *.sig && \
  rm -rvf *.tar.* && \
  cd ../keepmenu && \
  rm -rvf *.tar && \
  rm -rvf *.zst && \
  rm -rvf *.tar.* && \
  rm -rvf pkg && \
  rm -rvf src && \
  cd ../mailspring && \
  rm -rvf *.tar && \
  rm -rvf *.zst && \
  rm -rvf *.tar.* && \
  rm -rvf *.deb && \
  rm -rvf pkg && \
  rm -rvf src && \
  cd ../steinberg-asio && \
  rm -rvf pkg && \
  rm -rvf src && \
  cd ../../
